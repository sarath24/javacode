package main;
import java.time.LocalDate;
import java.util.Objects;
import java.util.Scanner;
public  class Bike implements Comparable<Bike> {
	private String company;
	private String model;
	private Color color;
	private int prize;
	private LocalDate releaseDate;
	
public Bike(String company,String model,int prize,Color color,LocalDate releaseDate) {
	this.company=company;
	this.model=model;
	this.prize=prize;
	this.color=color;
	this.releaseDate=releaseDate;
}
public String getCompany() {
	return company;
}
public void setCompany(String company) {
	this.company = company;
}
public String getModel() {
	return model;
}
public void setModel(String model) {
	this.model = model;

}

public Color getColor() {
	return color;
}
public void setColor(Color color) {
	this.color = color;
}
public int getPrize() {
	return prize;
}
public void setPrize(int prize) {
	this.prize = prize;
}
public LocalDate getReleaseDate() {
	return releaseDate;
}
public void setReleaseDate(LocalDate releaseDate) {
	this.releaseDate = releaseDate;
}
public Bike() {
}
public String toString()
{
   return new
		   StringBuffer().append("COMPANY:").append(company).append("\n").append("MODEL").append(model).append("\n").append("PRIZE:").append(prize).append("\n").append("COLOR:").append(color).append("\n").append("DATE:").append(releaseDate).toString();
}

@Override
public boolean equals(Object obj) {
	if (this == obj)
		return true;
	if (obj == null)
		return false;
	if (getClass() != obj.getClass())
		return false;
	Bike other = (Bike) obj;
	return Objects.equals(color, other.color) && Objects.equals(company, other.company)
			&& Objects.equals(model, other.model) && prize == other.prize
			&& Objects.equals(releaseDate, other.releaseDate);
}
public void readb() {
	Scanner scanner=new Scanner(System.in);

	System.out.println("Enter The   Company");
	company= scanner.next();
	System.out.println("Enter The  Model");
	model=scanner.next();
	System.out.println("Enter The Prize");
	prize=scanner.nextInt();
	System.out.println("Enter The color");
	String k=scanner.next();
	color=Color.valueOf(k);
	System.out.println("Enter The Releasdate");
	releaseDate=LocalDate.parse(scanner.next());
	
}

@Override
public int compareTo(Bike bike) {
	return prize-bike.prize;
//	if(prize<bike.prize)
//		return -1;
//		else if(prize>bike.prize)
//			return 1;
//		else
//			return 0;
}

}

