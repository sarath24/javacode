package main;
import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.Arrays;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Scanner;
/**
* A interface specifies the operations on Bike object object
* @author sarath
*
*
*/
public interface BikeService {
	/**
	* The method will add a Bike
	* @param Bike bike to be added
	*
	*/
 void add(Bike bike) ;
/**
* The method should display all the Bikes
*/
 void displayAll();
/**
  void 
* The method will search Bike and display the Bike with given company
* @param comp company of the student to be searched
*
*/
 
void search(String name);
/**
* The method will search Bikes and display the Bike with releaseDate is after the given releaseDate
* @param releaseDate Date of  releasing Bike
 * @return 
*
*/
List<Bike> search(LocalDate date);
/**
* The method sorts the Bikes based on prize
*/
void sort();

default  String getCSVString(Bike bike) {
	String result;
	StringBuffer buffer1=new StringBuffer();
	buffer1.append("\"").append(bike.getCompany()).append("\",").append(bike.getModel()).append("\",\"").append(bike.getPrize()).append("\",\"").append(bike.getColor()).append("\",\"").append(bike.getReleaseDate());
	result=buffer1.toString();
	return result;
		
}

void downloadDetailsAsCSV() throws IOException;

default  String getJSONString(Bike bike) {
	return new StringBuffer().append("{").append("\"Company\":").append("\"").append(bike.getCompany()).append("\"").append(", ").append("\"Model\":").append("\"").append(bike.getModel()).append("\"").append(", ").append("\"Prize\":").append(bike.getPrize()).append(", ").append("\"Color\":").append("\"").append(bike.getColor()).append("\"").append(", ").append("\"Date\":").append(bike.getReleaseDate()).append("}").toString();
}
void downloadDetailsAsJSON()throws IOException;

void delete(String name);

}
