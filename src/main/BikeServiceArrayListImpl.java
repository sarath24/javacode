package main;

import java.io.FileWriter;
import java.io.IOException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;
import java.util.StringJoiner;
import java.util.stream.Collectors;

public class BikeServiceArrayListImpl implements BikeService {
	List<Bike> bikeArryList=new ArrayList<>();
    
   
    
	@Override
	public void add(Bike bike) {
		int k=0;
		if(bikeArryList.size()>0&& bikeArryList.contains(bike)){
			System.out.println("Already Exist");

			}else {
			bikeArryList.add(bike);
			}
			k++;
			}
		
	
	@Override
	public void displayAll() {
		bikeArryList.forEach(System.out::println);
		//bikeArryList.forEach(element->System.out.println(":::\n"+element+":::"));
		}
	

	@Override
	public void search(String name) {
		for(Bike element:bikeArryList) {
			if(element.getCompany().equalsIgnoreCase(name)) {
				System.out.println(element);
		}
		}

        }

	@Override
	public List<Bike>  search(LocalDate date) {
		//List<Bike> bikelist=new ArrayList<>();
		return bikeArryList.stream().filter(bike -> bike.getReleaseDate().isAfter(date))
				.collect(Collectors.toList());
		
	}

	@Override
	public void sort() {
	Collections.sort(bikeArryList);
	displayAll();

	}

	@Override
	public void downloadDetailsAsCSV() throws IOException {
	
		FileWriter filewriter=new FileWriter("BikeServicecImpl.csv");
		
		for (int i = 0; i < bikeArryList.size(); i++) {
			String res=getCSVString(bikeArryList.get(i));
			System.out.println(res);
			filewriter.write(res);
	
		}
		filewriter.close();
	}

	@Override
	public void downloadDetailsAsJSON()throws IOException {
		String str;
		StringJoiner join = new StringJoiner(",", "[", "]");
		FileWriter file = new FileWriter("download.json");
		for (Bike bike : bikeArryList) {
		str = getJSONString(bike);
		join.add(str);
		}file.write(join.toString());
		file.close();
	}

	@Override
	public void delete(String name) {
		for (Iterator<Bike> iterator = bikeArryList.iterator(); iterator.hasNext();) {
			Bike bike = iterator.next();
			if(bike.getModel().equals(name)) {
			iterator.remove();
			}
			}
		
	}

}