package main;

import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.Date;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import java.util.StringJoiner;

public class BikeServiceDatabaseImpl implements BikeService {
String driver,user,password,url;

	public BikeServiceDatabaseImpl( String driver,String user, String password, String url) {
	this.driver = driver;
	this.user = user;
	this.password = password;
	this.url = url;
}
	  Connection getConnection() throws ClassNotFoundException, SQLException  {
		Class.forName(driver);
		return DriverManager.getConnection(url, user,password);
		
	}
private Bike getsBike(ResultSet resultSet)
{
	Bike bike=new Bike();
	  try {
	bike.setCompany(resultSet.getString("company"));
	bike.setModel(resultSet.getString("model"));
	bike.setPrize(resultSet.getInt("prize"));
	bike.setColor(Color.valueOf(resultSet.getString("color")));
	bike.setReleaseDate(resultSet.getDate("releaseDate").toLocalDate());
	
	}catch (SQLException e) {
	e.printStackTrace();
}
	return bike;
	
}
	@Override
	public void add(Bike bike) {
	Connection connection;
	
		try {
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("insert into bike1(company,model,prize,color,releasedate)values(?,?,?,?,?)");
			prestatement.setString(1,bike.getCompany() );
			prestatement.setString(2, bike.getModel());
			prestatement.setInt(3, bike.getPrize());
			prestatement.setString(4, bike.getColor().toString());
			prestatement.setDate(5,Date.valueOf(bike.getReleaseDate()));
			prestatement.executeUpdate();
			connection.close();
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	
	}

	
	@Override
	public void displayAll() {
		Connection connection;
		
		try {
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("select * from bike1");
	
			ResultSet resultSet=prestatement.executeQuery();
			while(resultSet.next()) {
				Bike bike = getsBike(resultSet);
			System.out.println(bike);
//				String company=result.getString("company");
//				String model=result.getString("model");
//				int prize=result.getInt("prize");
//				String color=result.getString("color");
//				Date releasedate=result.getDate("releasedate");
//				System.out.println("Company :"+company);
//				System.out.println("Model :"+model);
//				System.out.println("Prize :"+prize);
//				System.out.println("Color :"+ color);
//				System.out.println("Release	:"+releasedate);	
			}
			resultSet.close();
			prestatement.close();
			connection.close();
			
		} catch (ClassNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}

	}

	@Override
	public void search(String name) {
Connection connection;
		
		try {
			connection = getConnection();
			PreparedStatement prestatement=connection.prepareStatement("select * from bike1 where model=?");
			prestatement.setString(1,name);
	
			ResultSet resultSet=prestatement.executeQuery();
			while(resultSet.next()) {
				Bike bike = getsBike(resultSet);
				System.out.println(bike);	
			}
			resultSet.close();
			prestatement.close();
			connection.close();
			
		} catch (ClassNotFoundException e) {
			e.printStackTrace();
		} catch (SQLException e) {
			e.printStackTrace();
		}

	}

	@Override
	public List<Bike> search(LocalDate date) {
		List<Bike> bikeList=new ArrayList<>();
		try {
		Connection connection = getConnection();
		connection = getConnection();
		PreparedStatement preparedStatement = connection.prepareStatement("SELECT * FROM bike1 where releaseDate>?;");
		preparedStatement.setDate(1,Date.valueOf(date));
		ResultSet resultSet=preparedStatement.executeQuery();
		while(resultSet.next())
		{
		bikeList.add(getsBike(resultSet));
		}
		connection.close();
		} catch (ClassNotFoundException | SQLException e) {
		e.printStackTrace();
		}


		return bikeList;
	}

	


	

	@Override
	public void sort() {
		Connection connection;
		try {
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("select * from bike1 order by company");
		ResultSet resultSet = prestatement.executeQuery();
		while (resultSet.next()) {
		Bike bike = getsBike(resultSet);
		System.out.println(bike);



		}
		resultSet.close();
		connection.close();



		} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}
		}
		
	@Override
	public void downloadDetailsAsCSV() throws IOException {
		Connection connection;
		try {
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("select * from bike1");
		ResultSet resultSet = prestatement.executeQuery();
		FileWriter file = new FileWriter(" BikeServiceDatabase.csv");
		while (resultSet.next()) {

		String data =getCSVString(getsBike(resultSet));
		file.write(data);
		}
		file.close();
		} catch (ClassNotFoundException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		} catch (SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}

	}

	@Override
	public void downloadDetailsAsJSON() throws IOException {
		Connection connection;



		try {
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("select * from bikenew");
		ResultSet resultSet = prestatement.executeQuery();
		FileWriter file = new FileWriter("BikeServiceDatabase.json");
		StringJoiner join = new StringJoiner(",", "[", "]");
		while (resultSet.next()) {
		String data = join.add(getJSONString(getsBike(resultSet))).toString();
		file.write(data);



		}
		file.close();
		connection.close();
		} catch (ClassNotFoundException | SQLException e) {
		// TODO Auto-generated catch block
		e.printStackTrace();
		}

	}

	@Override
	public void delete(String name) {
		Connection connection;
		try {
		connection = getConnection();
		PreparedStatement prestatement = connection.prepareStatement("DELETE from bike1 where company=?");
		prestatement.setString(1, name);
		prestatement.executeUpdate();
		System.out.println("Successfully Deleted");
        connection.close();
       } catch (ClassNotFoundException e) {
		
		e.printStackTrace();
		} catch (SQLException e) {
	
		e.printStackTrace();
		}

	}

		}
	
