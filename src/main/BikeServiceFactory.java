package main;

import java.util.Properties;

public abstract class BikeServiceFactory {
public static BikeService getBikeservice(String type,Properties properties) {
	//String type = properties.getProperty("implementation");
	//type =type.toUpperCase();
	BikeService service=null;
	switch(type) {
	case "Array":
	service= new BikeServiceArrayImpl();
		break;
	case "ArrayList":
		service= new BikeServiceArrayListImpl();
	break;
	case "Database":
		String driver=properties.getProperty("driver");
		String user=properties.getProperty("user");
		String password=properties.getProperty("password");
		String url=properties.getProperty("url");
		service= new BikeServiceDatabaseImpl(driver, user, password, url);
		
	}
	return service;
}
}
