package main;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.time.LocalDate;
import java.util.InputMismatchException;
import java.util.List;
import java.util.Properties;
import java.util.Scanner;

public class Main {
	public static void main(String[] args) throws IOException {
		
		Scanner scanner=new Scanner(System.in);
		FileReader reader=new FileReader("src/config.properties");
		Properties properties=new Properties();
		properties.load(reader);
		//BikeService bikeServices=new BikeServiceArrayImpl();
		//BikeService bikeServices=new BikeServiceArrayListImpl();
		BikeService bikeServices=BikeServiceFactory.getBikeservice("Array",properties);
		   
		    while(true) {
	        	System.out.println("1.Add bike");
	    		System.out.println("2.Display details");
	    		System.out.println("3.Search");
	    		System.out.println("4.Search By Date");
	    	    System.out.println("5.Sort");
	    		System.out.println("6.Download csv file");
	    		System.out.println("7.Download JSON file");
	    		System.out.println("8.Delete");
	    		System.out.println("9.Exit");
	    		System.out.println("------------------------");
	    	    System.out.println("Enter the choice");
	        	 int c=scanner.nextInt();
	        	 System.out.println("------------------------");
	        	 
			switch(c)
			{
			
			    case 1: 
			    	try {
					System.out.println("Enter The   Company");
					String company = scanner.next();
					System.out.println("Enter The  Model");
					String model = scanner.next();
					System.out.println("Enter The Prize");
					int prize = scanner.nextInt();
					System.out.println("Enter The color");
					String l = scanner.next();
					Color color=Color.valueOf(l.toUpperCase());
					System.out.println("Enter The Releasdate");
					LocalDate releaseDate = LocalDate.parse(scanner.next());
					 Bike b1=new Bike(company,model,prize,color,releaseDate);
					 bikeServices.add(b1);
			}
			catch(InputMismatchException company) {
				System.out.println("Invalid Input");
			}
			System.out.println("Continue");
			
			     break;
	             case 2:
	            	 bikeServices.displayAll();
	            	 break;
                    case 3:
                    	System.out.println("Enter The  Search");
                    	String name=scanner.next();
                        bikeServices.search(name);
    	           break;
	              case 4:
	            	  System.out.println("Enter Dob of player To Be Searched\n");
	  		        String date=scanner.next();
	  		        LocalDate releasedate=LocalDate.parse(date);
	  		        List<Bike> bike=bikeServices.search(releasedate);
	  		        System.out.println(bike);
	  		        if(bike==null)
	  		        System.out.print("No Data Found");
	  				break;
	              case 5:
	            	  bikeServices.sort();
	            	  break;
	            	  
	              case 6:
	            	  try {
	            	  bikeServices.downloadDetailsAsCSV();
	            	  }
	      			catch(IOException e) {
	      				e.printStackTrace();
	      			}	
	            	  break;
	              case 7:
	            	  try {
	            	  bikeServices.downloadDetailsAsJSON();
	            	  }
		      			catch(IOException e) {
		      				e.printStackTrace();
		      			}	
	            	  break;
	              case 8:
	            	  System.out.println("Enter The data");
                  	String name1=scanner.next();
	            	  bikeServices.delete(name1);
                  	
  	           break;
	            	  
	              case 9:
    	        System.out.println("Exited");
    	        System.exit(0);
    default :
    	System.out.println("Invalid Entry");
    	break;
	}
}
}
	}

