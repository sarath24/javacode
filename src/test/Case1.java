package test;

import static org.junit.jupiter.api.Assertions.*;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;

import org.junit.jupiter.api.Test;

import main.Bike;
import main.BikeServiceArrayImpl;
import main.Color;

public class Case1 {
	BikeServiceArrayImpl bikeServiceArrayImpl=new BikeServiceArrayImpl(3);
	@Test
	void test() {
		
		bikeServiceArrayImpl.add(new Bike("hero","passion",7600,Color.valueOf("RED"),LocalDate.parse("2014-10-10")));
		assertEquals(1,bikeServiceArrayImpl.getCount());
		bikeServiceArrayImpl.add(new Bike("hero","passion",7600,Color.valueOf("RED"),LocalDate.parse("2014-10-10")));
		assertEquals(1,bikeServiceArrayImpl.getCount());
	
	}

}
